"""Graph statistics about your tea consumption."""

import argparse
import collections
import datetime
import os

import matplotlib.font_manager
import matplotlib.pyplot as plt

import history
import record


def _get_brand_dict(records: list):
    """
    Returns:
        occurrences (dict): Dict with key of brand and value of list of
            datetime.datetimes.
    """
    brand_occurrences = collections.defaultdict(list)

    for rec in records:
        brand_occurrences[rec.teabrand].append(rec.datetime)

    return brand_occurrences


def _get_teaname_dict(records: list):
    """
    Returns:
        occurrences (dict): Dict with key of teaname and value of list of
            datetime.datetimes.
    """
    tea_occurrences = collections.defaultdict(list)
    for rec in records:
        tea_occurrences[rec.name].append(rec.datetime)

    return tea_occurrences


def _get_top_entries(occurrences: dict, limit: int):
    """
    Args:
        limit (int): The number of top results to return.
            If limit is zero, all elements are included in `top`.
        occurrences (dict): Dict with key of name and value of list of
            datetime.datetimes.
    Returns:
        top (list) Keys from `occurrences` for the top `limit` elements, in
            descending order.
        other (list) The combined (and sorted) datetime.datetimes from each key
            not in `top`.
    """
    num_records = {
        key: len(records) for key, records in occurrences.items()
    }
    top_keys = sorted(num_records, key=num_records.get, reverse=True)

    if limit == 0:
        return top_keys, []

    top_n, other_keys = top_keys[:limit], top_keys[limit:]

    # Combine all other records into one
    other = []
    for key in other_keys:
        other.extend(occurrences[key])
    other = sorted(other)

    return top_n, other


def plot_line(occurrences: dict, limit: str, title: str):
    """
    Create a line graph of number of cups vs Date.

    Args:
        occurrences (dict): Dict with key of name and value of list of
            datetime.datetimes.
        limit (int): The number of top results to plot. All those outside top
            are plotted under 'Other'.
        title (str): Title of the graph
    """
    top_n, other = _get_top_entries(occurrences, limit)

    for key in top_n:
        dates = occurrences[key]
        plt.plot(
            dates, range(1, len(dates) + 1), label=key,
            linewidth=2,  marker='.', markersize=10
        )

    plt.plot(
        other, range(1, len(other) + 1), label='Other', linewidth=2,
        marker='.', markersize=10
    )

    plt.grid()
    plt.title(title)
    plt.ylabel('Cups')
    plt.xlabel('Date')
    plt.ylim(0)

    # Use small font
    fontP = matplotlib.font_manager.FontProperties()
    fontP.set_size('small')
    plt.legend(prop=fontP, loc='upper left')


def plot_share(occurrences: dict, limit: str, title: str):
    """
    Create a pie chart of number of cups vs Date.

    Args:
        occurrences (dict): Dict with key of name and value of list of
            datetime.datetimes.
        limit (int): The number of top results to plot. All those outside top
            are plotted under 'Other'.
        title (str): Title of the graph
    """
    top_n, other = _get_top_entries(occurrences, limit)

    sizes = [len(occurrences.get(key)) for key in top_n]
    sizes.append(len(other))

    top_n.append('Other')

    _, _, autotexts = plt.pie(
        sizes, labels=top_n, autopct='%1.1f%%', startangle=90
    )
    # Make percent white (except for entries coloured white)
    for idx, autotext in enumerate(autotexts):
        # Standard colours for matplotlib are:
        #   ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
        if (idx + 1) % 8 != 0:
            autotext.set_color('white')

    plt.title(title)

    # Use small font
    fontP = matplotlib.font_manager.FontProperties()
    fontP.set_size('small')
    plt.legend(prop=fontP)


def plot_brand_line(records: list, brands: set, limit: int):
    """Graph number of cups over time for each brand."""
    brand_occurrences = _get_brand_dict(records=records)

    plot_line(brand_occurrences, limit, title='Brand Popularity')


def plot_teaname_line(records: list, tea_names: set, limit: int):
    """Graph number of cups over time for each unique tea."""
    teaname_occurrences = _get_teaname_dict(records=records)

    plot_line(teaname_occurrences, limit, title='Tea Popularity')


def plot_brand_share(records: list, brands: set, limit: int):
    """Create pie chart showing share of cups for each brand."""
    brand_occurrences = _get_brand_dict(records=records)

    plot_share(brand_occurrences, limit, 'Brand Share')


def plot_teaname_share(records: list, tea_names: set, limit: int):
    """Create pie chart showing share of cups for each unique tea."""
    teaname_occurrences = _get_teaname_dict(records=records)

    plot_share(teaname_occurrences, limit, 'Tea Share')


def plot_average(records: list):
    """Create line graph showing average cups per day over time."""
    dates = [rec.datetime for rec in records]
    plt.plot(
        dates, _moving_average(records),
        label='all'
    )
    plt.plot(
        dates, _moving_average_window(records, window=30),
        label='30 days'
    )

    plt.grid()
    plt.legend(loc='best')
    plt.title('Running Mean')
    plt.ylabel('Cups per day')
    plt.xlabel('Date')


def _moving_average(records: list):
    running_mean = []

    start_date = records[0].datetime

    for idx, rec in enumerate(records):
        date = rec.datetime
        days_elapsed = (date - start_date).days + 1
        mean = (idx + 1) / days_elapsed

        running_mean.append(mean)

    return running_mean


def _moving_average_window(records: list, window: int = 30):
    """Calculate moving average of the last `window` days."""
    running_mean = []

    start_date = records[0].datetime
    start_pos = 0

    for idx, rec in enumerate(records):
        start_date = rec.datetime - datetime.timedelta(window)
        while records[start_pos].datetime < start_date:
            start_pos += 1

        count = idx - start_pos + 1
        mean = count / window

        running_mean.append(mean)

    return running_mean


def _parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'path', type=str, help='Path to history file'
    )
    parser.add_argument(
        '-a', '--average', action='store_true',
        help='Plot running mean of cups per day'
    )
    parser.add_argument(
        '-bl', '--brandline', action='store_true',
        help='Plot cups over time for each brand'
    )
    parser.add_argument(
        '-bs', '--brandshare', action='store_true',
        help='Plot share for each brand'
    )
    parser.add_argument(
        '-tl', '--tealine', action='store_true',
        help='Plot cups over time for each unique tea'
    )
    parser.add_argument(
        '-ts', '--teashare', action='store_true',
        help='Plot share for each unique tea'
    )
    parser.add_argument(
        '-l', '--limit', default=6, type=int,
        help=('Number of entries to plot for tea name/brand. '
              '0 for all entries. (default: 6)')
    )

    args = parser.parse_args()

    return args


if __name__ == '__main__':
    args = _parse_args()

    if os.path.splitext(args.path)[1] == '.csv':
        hist = history.HistoryCSV(path=args.path)
    else:
        raise ValueError(
            'Unknown history store: "{}"'.format(args.path)
        )

    records = hist.read_history()

    brands = record.get_tea_brands(records)
    names = record.get_tea_names(records)

    if args.average:
        plot_average(records=records)
    if args.brandline:
        plot_brand_line(records=records, brands=brands, limit=args.limit)
    if args.brandshare:
        plot_brand_share(records=records, brands=brands, limit=args.limit)
    if args.tealine:
        plot_teaname_line(records=records, tea_names=names, limit=args.limit)
    if args.teashare:
        plot_teaname_share(records=records, tea_names=names, limit=args.limit)

    # Print the average anyway
    start_date = records[0].datetime
    end_date = records[-1].datetime
    days_elapsed = (end_date - start_date).days + 1
    mean = (len(records)) / days_elapsed
    print('Average per day: {:.3f}'.format(mean))

    plt.show()
