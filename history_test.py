import datetime

import pytest

import history
import record

@pytest.mark.parametrize('tea,time,extras,expected', [
    (  # all provided
        'Test:Name', datetime.datetime(2001, 6, 19, 14, 30),
        ['Sugar', 'Honey'], '19/06/2001,14:30,Test:Name,Sugar,Honey'
    ),
    (  # Only one extra
        'Test:Name', datetime.datetime(2001, 6, 19, 14, 30), ['Sugar'],
        '19/06/2001,14:30,Test:Name,Sugar'
    ),
    (  # No extras
        'Test:Name', datetime.datetime(2001, 6, 19, 14, 30), [],
        '19/06/2001,14:30,Test:Name'
    ),
])
def test_CSV_create_record(tea, time, extras, expected):
    hist = history.HistoryCSV(path='test')

    rec = record.TeaRecord(name=tea, time=time, extras=extras)

    out = hist._create_record(rec)
    assert out == expected
