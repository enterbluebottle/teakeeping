"""Module contains classes which interact with tea history data stores."""
import abc
import datetime
import os

import record


class History(abc.ABC):
    """DAO for tea history records."""

    def read_history(self):
        """
        Read tea history and extract all teas drunk in the past.

        Returns:
            set: names of all teas drunk in history.

        """
        raise NotImplementedError

    def write_history(self, tea: record.TeaRecord):
        """
        Add a new tea record to the history.

        It is assumed the tea was drunk today.

        Args:
            tea (TeaRecord): the tea name with the time drunk and extras.

        """
        raise NotImplementedError


class HistoryCSV(History):
    """
    Read and store tea records from a CSV file.

    Args:
        path (str): path to the CSV file.

    """

    def __init__(self, path: str):
        self.path = path
        self._date_format = '%d/%m/%Y'
        self._time_format = '%H:%M'

    def read_history(self):
        """
        Read a tea history file and extract all teas drunk in the past.

        Returns:
            set: names of all teas drunk in history.

        """
        # History file does not currently exist; no history to read.
        if not os.path.exists(self.path):
            return []

        with open(self.path, 'r') as hist_file:
            tea_records = []

            for line in hist_file:
                date_str, time_str, tea_name, *extras = line.strip().split(',')

                time = self._get_datetime_from_str(date_str, time_str)

                rec = record.TeaRecord(
                    name=tea_name, time=time, extras=extras
                )
                tea_records.append(rec)

        return tea_records

    def write_history(self, tea: record.TeaRecord):
        """Append a new `tea` to the history file."""
        record = self._create_record(tea=tea)

        with open(self.path, 'a') as hist_file:
            hist_file.write(record + '\n')

        print(record)

    def _create_record(self, tea: record.TeaRecord):
        """
        Format inputs to CSV in preparation for writing to our output.

        Args:
            tea (TeaRecord): the tea name with the time drunk and extras.

        Returns:
            str: the formatted tea record.

        """
        date_str = tea.datetime.strftime(self._date_format)
        time_str = tea.datetime.strftime(self._time_format)

        # Make sure there are no spaces in the tea
        tea_name = tea.name.replace(' ', '_')

        # Record stored in CSV format
        record = ','.join([date_str, time_str, tea_name])

        # Extras added separately, to avoid trailing comma if none are given.
        if tea.extras:
            record += ',' + ','.join(tea.extras)

        return record

    def _get_datetime_from_str(self, date: str, time: str):
        datetime_str = '{}T{}'.format(date, time)
        format_str = '{}T{}'.format(self._date_format, self._time_format)
        return datetime.datetime.strptime(datetime_str, format_str)
