#!/usr/bin/env python3

"""Track the tea you drink."""

import argparse
import datetime
import os
import random
import readline
import sys
from typing import Iterable

import toml

import history
import record


def load_stocked_teas(path: str):
    """Return list of all teas in stock, read from a TOML file at `path`."""
    if not path:
        return []

    with open(path, 'r') as f:
        return toml.load(f)["in_stock"]


def parse_tea_input(inp_str: str):
    """
    Extract details of the tea from an input string.

    Date is always assumed to be today. If time is not given, it is expected
    to be now.

    Args:
        inp_str (str): The string the user inputted, in the form:
            'tea[;EXTRAS][;TIME]'

    Returns:
        TeaRecord

    Raises:
        ValueError: input was invalid.

    """
    inp = inp_str.split(';')

    tea = inp[0]

    if not tea:
        raise ValueError

    if len(inp) >= 2 and inp[1]:
        extras = inp[1].split(',')
    else:
        extras = []

    # Assumed that time is now, unless a time is specified
    time = datetime.datetime.now()
    if len(inp) >= 3 and inp[2]:
        hour, minute = inp[2].split(':')
        time = time.replace(hour=int(hour), minute=int(minute), second=0)

    return record.TeaRecord(name=tea, time=time, extras=extras)


def _configure_completer(words: Iterable[str]):
    """Autocomplete tea names from history."""
    def completer(text: str, state: int):
        for word in words:
            if word.startswith(text):
                if not state:
                    return word
                else:
                    state -= 1

    # Our teas will be of form Brand:Name, so can't use colon as delimiter
    new_delims = readline.get_completer_delims().replace(':', '')
    readline.set_completer_delims(new_delims)

    readline.parse_and_bind("tab: complete")
    readline.set_completer(completer)


def get_input(in_stock: list):
    """
    Get the tea the user just drunk.

    Will loop until the user adds a new tea record.

    Return:
        TeaRecord

    """
    last_random = None

    while True:
        prompt = "Enter tea (or 'help' for options; nothing for suggestion): "
        inp = input(prompt)

        inp = inp.strip()

        if inp.lower() in ['help', 'h']:
            print_help()

        elif inp == '' or inp.lower().split(' ')[0] == 'r':
            last_random = random.choice(in_stock)
            print(last_random)

        elif inp.strip() == '!!':
            # Use the latest suggestion
            if last_random is None:
                print('Error: have not suggested a tea')
            else:
                return record.TeaRecord(
                    name=last_random, time=datetime.datetime.now(), extras=[]
                )

        else:
            return parse_tea_input(inp)


def print_help():
    """Print the help message."""
    print('Entry format: tea[;EXTRAS][;TIME]')
    print('Examples:')
    print('     > TeaBrand:TeaType;Sugar;15:00')
    print('     > TeaBrand:TeaType;;10:00')
    print('     > TeaBrand:TeaType;Milk')
    print('     > TeaBrand:TeaType;Honey,Lemon')
    print('     > TeaBrand:TeaType')
    print()
    print('You can get a tea suggestion by leaving the line blank or typing:')
    print('     > r')
    print('If you want to use your suggestion, enter "!!" at the next prompt.')


def _parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'history_file', type=str,
        help='Path to a file in which tea records are kept.'
    )
    parser.add_argument(
        '-s', '--stock', type=str,
        help='Path to the TOML file in which details currently available teas.'
    )

    return parser.parse_args()


def main():
    args = _parse_args()

    if os.path.splitext(args.history_file)[1] == '.csv':
        hist = history.HistoryCSV(path=args.history_file)
    else:
        raise ValueError(
            'Unknown history store: "{}"'.format(args.history_file)
        )

    records = hist.read_history()
    _configure_completer(record.get_tea_names(records))

    stock = load_stocked_teas(path=args.stock)

    try:
        tea_record = get_input(in_stock=stock)
    except KeyboardInterrupt:
        print('Exiting')
        sys.exit(0)

    hist.write_history(tea=tea_record)


if __name__ == '__main__':
    main()
