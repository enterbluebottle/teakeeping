import datetime

import freezegun
import pytest

import teakeeping


@pytest.mark.parametrize('input,exptea,exptime,expextra', [
    # just tea
    ('T:Name', 'T:Name', datetime.datetime(2001, 6, 19, 11, 29, 12), []),
    (  # one extra
        'T:Name;Sugar', 'T:Name', datetime.datetime(2001, 6, 19, 11, 29, 12),
        ['Sugar']),
    (  # 2 extras
        'T:Name;Sugar,Lemon', 'T:Name',
        datetime.datetime(2001, 6, 19, 11, 29, 12), ['Sugar', 'Lemon']
    ),
    (  # extras & time
        'T:Name;e1,e2;14:00', 'T:Name', datetime.datetime(2001, 6, 19, 14, 0),
        ['e1', 'e2']
    ),
    (  # tea and time
        'T:Name;;15:14', 'T:Name', datetime.datetime(2001, 6, 19, 15, 14), []
    ),
    # Ignore extra fields
    (
        'T:Name;e1;14:00;ignore', 'T:Name',
        datetime.datetime(2001, 6, 19, 14, 00), ['e1']
    ),
])
def test_parse_tea_input(input, exptea, exptime, expextra):
    with freezegun.freeze_time('2001-06-19 11:29:12'):
        out = teakeeping.parse_tea_input(inp_str=input)

    assert out.name == exptea
    assert out.datetime == exptime
    assert out.extras == expextra


def test_part_tea_input_error():
    with pytest.raises(ValueError):
        teakeeping.parse_tea_input('')

