"""Represents a tea that has been consumed."""
import datetime


class TeaRecord(object):
    """
    Object that represents a tea that has been consumed.

    Args:
        name (str): The 'TeaBrand:TeaType' of the tea.
        time (datetime.datetime): Date & time at which the tea was
            consumed.
        extras (list(str)): extras with which the tea was consumed.

    Attributes:
        name (str): The 'TeaBrand:TeaType' of the tea.
        teabrand (str): brand of tea consumed.
        teatype (str): type of tea consumed.
        extras (list(str)): extras with which the tea was consumed.
        datetime (datetime.datetime): Date & time at which the tea was
            consumed.

    """

    def __init__(self, name: str, time: datetime.datetime, extras: list = []):
        self.name = name
        self.teabrand, self.teatype = name.split(':')
        self.extras = extras
        self.datetime = time


def get_tea_names(tea_records: list):
    """Extract all unique TeaBrand:TeaName values from a list of TeaRecords."""
    return set([__.name for __ in tea_records])


def get_tea_brands(tea_records: list):
    """Extract all unique TeaBrand values from a list of TeaRecords."""
    return set([__.teabrand for __ in tea_records])
