import datetime

import pytest

import record

def test_get_tea_names():
    recs = [
        record.TeaRecord('Brand1:Type1', datetime.datetime.now(), []),
        record.TeaRecord('Brand2:Type1', datetime.datetime.now(), []),
        record.TeaRecord('Brand1:Type2', datetime.datetime.now(), []),
        record.TeaRecord('Brand1:Type1', datetime.datetime.now(), []),
    ]

    expected = set(['Brand1:Type1', 'Brand2:Type1', 'Brand1:Type2'])
    assert record.get_tea_names(tea_records=recs) == expected


def test_get_tea_brands():
    recs = [
        record.TeaRecord('Brand1:Type1', datetime.datetime.now(), []),
        record.TeaRecord('Brand2:Type1', datetime.datetime.now(), []),
        record.TeaRecord('Brand1:Type2', datetime.datetime.now(), []),
        record.TeaRecord('Brand1:Type1', datetime.datetime.now(), []),
    ]

    expected = set(['Brand1', 'Brand2'])
    assert record.get_tea_brands(tea_records=recs) == expected
