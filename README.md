# Teakeeping

Teakeeping is a simple tool to keep track of the teas you're drinking, when you drank them and what you drank them with.

Every time you drink a tea, run `python teakeeping.py PATH/TO/HISTORY.csv`. Enter the tea you drank, when you drank (defaults to now) and what you drank it with (e.g. sugar, lemon). This will be saved to your history file (currently history is only stored in CSV format).

## In Stock Teas and Recommendations

You can optionally give a path to a file which stores what teas you have available to drink. When entering a tea, you can press 'r' to get a random suggestion. To use this feature, specify `-s` on the command line.

This 'stock' file is a TOML file, consisting of one key "in_stock", for which the value is an array of teas you have to hand. e.g.
```toml
in_stock = [
    "TeaBrand1:TeaName1",
    "TeaBrand2:TeaName2",
    "TeaBrand3:TeaName3",
]
```

## Graphing

You can graph statistics about your tea consumption using `graph.py`. See the command line help for which graphs you can generate.

## Dependencies

## Setting up

Activate the Python virtual environment and install dependencies:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```
If you add any new dependencies, update `requirements.txt`:

```bash
pip freeze > requirements.txt
```
